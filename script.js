const player1 = videojs("v1");
const player2 = videojs("v2");

const DUBAI =
  "https://dmisxthvll.cdn.mgmlcdn.com/dubaitvht/smil:dubaitv.stream.smil/playlist.m3u8";
const RED_BULL_TV =
  "https://rbmn-live.akamaized.net/hls/live/590964/BoRB-AT/master_3360.m3u8";

player1.src({ src: DUBAI });
player1.ready(() => {
  player1.play();
});

player2.src({ src: RED_BULL_TV });
player2.ready(() => {
  player2.play();
});

v2.addEventListener("click", function () {
  let src1 = player1.src();
  let src2 = player2.src();

  player1.src({ src: src2 });
  player1.ready(() => {
    player1.play();
  });

  player2.src({ src: src1 });
  player2.ready(() => {
    player2.play();
  });
});
