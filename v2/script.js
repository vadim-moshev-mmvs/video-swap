const clog = (...args) => console.log(...args);

const DUBAI =
  "https://dmisxthvll.cdn.mgmlcdn.com/dubaitvht/smil:dubaitv.stream.smil/playlist.m3u8";
const RED_BULL_TV =
  "https://rbmn-live.akamaized.net/hls/live/590964/BoRB-AT/master_3360.m3u8";

const player1 = videojs("v1");
const player2 = videojs("v2");

player1.src({ src: DUBAI });
player1.ready(() => {
  player1.play();
});

player2.src({ src: RED_BULL_TV });
player2.ready(() => {
  player2.play();
});

smallPlayerContainer.addEventListener("click", function () {
  const currentPlayer = currentPlayerContainer.querySelector("div");
  const smallPlayer = smallPlayerContainer.querySelector("div");

  currentPlayer.classList.remove("current");

  smallPlayerContainer.appendChild(currentPlayer);

  currentPlayerContainer.appendChild(smallPlayer);
  smallPlayer.classList.add("current");
});
